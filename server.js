const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');
const config = require('./config.json');
var multer = require('multer');
const cookieParser = require('cookie-parser');
fileupload = require("express-fileupload");
const MongoStore = require('connect-mongo')(session);
const helmet = require('helmet')
const path = require('path')
const fs = require('fs');
const cors = require('cors');


app.use(helmet())
app.use(cors())
app.use(fileupload())
app.use(cookieParser());


app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));


app.use(`/api/event`, require('./controllers/event.controller'));

app.listen(5000)