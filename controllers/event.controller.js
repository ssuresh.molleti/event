const express = require('express');
const router = express.Router();
const eventService = require('../services/event.service');

module.exports = router;

router.post('/calenderEvent', (req, res) => {
    let data = req.body;
    eventService.sendInvite(data).then((data) => {
        console.log("=================",data)
        res.send(data).status(200)
    }).catch((err) => {
        console.log(err);
        res.send(err).status(400)
    })
})